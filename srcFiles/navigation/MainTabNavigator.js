import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import PostScreen from '../screens/PostScreen';
import FullScreen from '../screens/FullScreen';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    PostView: PostScreen,
    FullScreen: FullScreen
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  swipeEnabled: true,
  tabBarVisible: false
};

HomeStack.path = '';

const tabNavigator = createBottomTabNavigator({
  HomeStack
});

tabNavigator.path = '';

export default tabNavigator;
