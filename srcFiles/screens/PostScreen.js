import React from 'react';
import { ScrollView, StyleSheet, View, Image, Platform, TouchableOpacity, Dimensions } from 'react-native';
import { Avatar, Button, Card, Title, Paragraph, Text } from 'react-native-paper';
import { WebView } from 'react-native-webview';

const PostComponent = (props) => {
  const data = props.navigation.getParam('post', {});  
  return (
    <View style={{height:'100%'}}>
    <WebView style={styles.container} source={{uri: `http://old.reddit.com/${data.permalink}`}}/>
    </View>
);
}
PostComponent.navigationOptions = {
  header: null,
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
    backgroundColor: '#fff',
  },
});

export default PostComponent;
