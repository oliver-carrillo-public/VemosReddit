import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import { Avatar, List, Divider } from 'react-native-paper';

class RedditList extends React.Component{

  _renderItem = (data) => {
    let post = data.item;
    const thumbnail = (!post.thumb || post.thumb === 'self') ? require('./../assets/images/self.png') : {uri: post.thumb};
    return (
      <List.Item title={post.title} description={`u/${post.author}  (+${post.votes})\n${post.commentCount} comments`}
        onPress={()=>{this.props.navigator('PostView', {post: post})}}
        onLongPress={()=>{this.props.navigator('FullScreen', {post: post})}}
        left={props => <Avatar.Image {...props} style = {{backgroundColor: styles.container.backgroundColor}} size={100} source = {thumbnail}/>}
       />
    );
  }

  _keyExtractor = (item) => { return item.id };

  render() {
    return (
      <FlatList
        ItemSeparatorComponent={Divider}
        renderItem={this._renderItem.bind(this)}
        keyExtractor={this._keyExtractor.bind(this)}
        data={this.props.data}
        refreshing={this.props.isRefreshing}
        onRefresh={this.props.update}
      />
    );
  }
}

export default class HomeScreen extends React.Component 
{
  state = {data: global.datasource.data, isRefreshing: false};

  componentDidMount()
  {
    global.datasource.events.on('data', (list)=>
    {
      console.log('Got a new list of content')
      this.setState({data: list, isRefreshing: false});
    });
  }

  async update()
  {
    console.log('Updating')
    this.setState({...this.state, isRefreshing: true});
    let newData = await global.datasource.fetch();
  }

  render()
  {
  return (
      <View style={styles.container}>
        <RedditList navigator={this.props.navigation.push} isRefreshing = {this.state.isRefreshing} data={this.state.data} update={()=>this.update()}/>
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
    title: 'Vemos Reddit View',
  };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
});
