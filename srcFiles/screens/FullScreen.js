import React from 'react';
import { StyleSheet, View, Image, Platform} from 'react-native';
import { Button, Card } from 'react-native-paper';
import {NavigationEvents} from 'react-navigation';
import PinchZoomView from 'react-native-pinch-zoom-view';

const FullScreen = (props) => {
  const data = props.navigation.getParam('post', {});  
  let conditionalBack = null;
  if (Platform.OS ==='ios')
  {
    conditionalBack = 
    <View style={{marginTop: 32, alignItems: 'center'}}>
      <Button icon="keyboard-arrow-left" color = {'black'} mode="contained" onPress={()=>{props.navigation.pop()}}>
        Back
      </Button>
    </View>;
  }
  return (
    <PinchZoomView style={styles.container} minScale={.25} maxScale={5}>
      <Image style={{aspectRatio: 1}} source={{uri: data.url}}/>
      {conditionalBack}
  </PinchZoomView>
);
}

FullScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
    backgroundColor: 'black',
  },
});

export default FullScreen;
