import React from 'react';
import {View, StyleSheet} from 'react-native'
import { ActivityIndicator, Colors } from 'react-native-paper';
const styles = StyleSheet.create({
  container: {
	flex: 1,
	backgroundColor: Colors.white, //'#fff	'
	justifyContent: 'center',
	alignItems: 'center'
  },
});

export default class LoadingScreen extends React.Component {
  static navigationOptions = null;

	componentDidMount()
	{
		console.log('Loading!')
		const { startAsync, onError, onFinish, postLoad } = this.props;
		if (startAsync) startAsync().then(onFinish).catch(onError);
	}

  render() {
	return (
	  <View style = {styles.container}>
		<ActivityIndicator animating={true} size="20" color={Colors.blue} />
	  </View>);
  }
}

