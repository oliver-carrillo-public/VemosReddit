import { Provider as PaperProvider } from 'react-native-paper'; //<-- React Native Paper addition
import AppLoading from './screens/Loading'; //<-- I've replaced the Expo-default loading scree`n with a functional equivalent
import RedditProvider from './utilities/redditProvider'; //<-- little custom data provider
global.datasource = new RedditProvider(); //<-- no real need to pass this around as a prop for such a simple application
import React, { useState } from 'react';
import { Platform, StatusBar, View, StyleSheet } from 'react-native';
import AppNavigator from './navigation/AppNavigator';

export default function App(props) 
{
	const [isLoadingComplete, setLoadingComplete] = useState(false); //some hooks
	let conditionalScreen = {};

	if (!isLoadingComplete && !props.skipLoadingScreen) 
	{
		conditionalScreen = 
			<AppLoading
				startAsync={loadResourcesAsync}
				onError={handleLoadingError}
				onFinish={() => handleFinishLoading(setLoadingComplete)}
			/>;
	}
	else 
	{
		conditionalScreen = <AppNavigator />
	}

	return( //wrap in some React-Native-Paper
		<PaperProvider> 
			{conditionalScreen}
		</PaperProvider>
		);
}

async function loadResourcesAsync() //Simplified drastically
{
	await Promise.all([
		global.datasource.fetch()
		//some other promises could go here
		]);
}

function handleLoadingError(Error) {
	console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
	setLoadingComplete(true);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
});
