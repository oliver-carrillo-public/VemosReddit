import React from 'react';
import { Image  } from 'react-native';
const Emitter = require('tiny-emitter');
const escapeHTML = require('unescape');

function formatter(str)
{
	//found some stray references to &amp; in the titles
	return escapeHTML(str);
}

function getfields(postList)
{
	let id = 0;
	return postList.map(rawPost=>
	{
		let post = rawPost.data;
		let author = ( ((post.author||'').indexOf('_') !== -1) ? post.author.split('_')[1] : post.author);
		let mappedPost = {
			url: (post.url||'').trim(),
			image: <Image style={{width: '100%', height: undefined, aspectRatio: 1}} source={{uri: post.url}}/>,
			author: formatter(author),
			title: formatter((post.title||'').trim()),
			votes: (post.score||0),
			thumb: (post.thumbnail||'').trim(),
			commentCount: (post.num_comments||0),
			permalink: post.permalink||'',
			id: id.toString()
		};
		id++;
		return mappedPost;
	}).filter(post=>{return (post.url && post.title)});
}


export default class Provider
{
	constructor(source)
	{
		this.data = [];
		this.source = source||'https://api.reddit.com/r/pics/hot.json';
		this.events = new Emitter();
	}

	async fetch()
	{
		console.log('Fetching from Reddit')
		try
		{
			let response = await fetch(this.source, { 
				headers: {
					'Cache-Control': 'no-cache, no-store, must-revalidate',
					'Pragma': 'no-cache',
	        		'Expires': 0
	        		}
	        	});

			if (!response) throw "Problem getting the Reddit content...";
			let raw = await response.json();
			if (raw && raw.data && Array.isArray(raw.data.children) && raw.data.children.length)
			{
				this.data = getfields(raw.data.children);
				this.events.emit('data', this.data);
				return {success: true};
			}
			else
			{
				console.warn('Problem in fetch()');
				return {success: false, message: "Nothing to see here..."};
			}
		}
		catch(err)
		{
			console.error(err);
			return {success: false, message: (typeof err === 'string' ? err : null)};
		}
	}
}