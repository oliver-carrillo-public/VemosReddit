# Foreward
Hey Patrick.  Hope you enjoy.  I'll explain what I've made here and tell you why I made the decisions I made.

## Time: Around three hours coding & tinkering, two hours testing on various devices and documenting this
###### -- An hour of that three hours was spent debugging the fact that I was using react-native's WebView instead of react-native-webview, which is community-maintained
###### -- (The WebView component from default RN does not play nice with Reddit's CSS and resulted in a bunch of overflowing panes and unreadable content.)

###### -- Instructions to launch via the command line are in the bottom section labeled "Running the app" => "Via React-Native-CLI"
###### -- I've included instructions to run on a physical device as well, using a couple methods
###### -- Tested with Expo on iOS/Android and Android Studio with Android (no Mac hardware to do a full build)

# Interesting Call-Outs
##### I've added some additional unrequested content:
###### -- Fallback and Reddit-specific Thumbnails/icons are getting pulled from Reddit, not just the r/pics JSON
###### -- Long press on images (in the expanded view) to view them in a zoomable view (pinch-to-zoom)
###### -- Pull down on the top of the screen to refresh!  (On the main list of Reddit posts, './screens/HomePage.js')
###### -- Custom loading screen with an animated indicator, easy enough!
###### -- It works on other subreddits, I tested a few
# Third-Party Frameworks / Libraries:
##### -- Expo: This project does not use Expo at all-- but I have included a version that launches from the Expo demo app
	Non-Expo: version without expo at (./VemosReddit.zip) -- pure React Native
##### -- React-Native-Paper: A Material-Design implementation for React Native (used here for some UI components)
##### -- React-Native-Navigation: The default navigation solution, pretty much boilerplate here
##### -- That's about it: I wrote most of the code from scratch
##### -- P.S. Making heavy use of ES6-style async/await -- Promises to the rescue
---

# Running the app
	==> Unzip the preferred package (either ./VemosReddit.zip or ./VemosRedditEXPO.zip) to a folder with the same name (minus .zip)
---
## Via React-Native-CLI (Without Expo)
###### From the root of the folder, navigate to ./VemosReddit
	npm install -g react-native react-native-cli 
		==> (Windows): your shell session may require a restart to register the new "expo" PATH variable
	[IOS]: cd ./VemosReddit/ios
	[Android]: cd ./VemosReddit/android
###### Using this directory, open the project in XCode or Android Studio:
	[IOS]: https://developers.facebook.com/docs/react-native/configure-ios/
	[Android]: just open this directory in Android Studio
###### Build the native dependencies
	[IOS]: using XCode, build as normal
	[Android]: using Android Studio, build as normal
###### Use the command line to launch
	[IOS]: react-native run-ios
	[Android]: react-native run-android

	==> This will scan your computer for attached devices and try to boot on them, either Emulated or USB
	==> You may need to enable Android develoepr mode for USB debugging, and USB connections may need a USB driver (ADB) 

---
## Running With EXPO
	Note: This requires Expo ("expo client") to be installed on the target device via its respective App Store
###### Navigate into the unzipped EXPO directory (./VemosRedditEXPO)
	cd ./VemosRedditEXPO
###### Run the command to install depdencies:
	npm install
	npm install -g expo expo-cli
		==> (Windows): your shell session may require a restart to register the new "expo" PATH variable

###### To boot up via USB in DEV mode (the computer may need a USB debugging driver, and the phone may need Developer mode enabled):
	[IOS]: expo start --ios --localhost --dev
	[Android]: expo start --localhost --android --dev
###### Production Mode:
	[IOS]: expo start --ios --localhost --no-dev
	[Android]: expo start --localhost --android --no-dev
###### To boot up via Wireless Network (using Expo app) in DEV mode
###### -- Connect both the host machine and target machine to the same network, then:
	expo start --dev
###### -- Or production mode:
	expo start --no-dev
###### then scan the QR code generated in the console with the Expo client.  The app will be built and transmitted over the wireless network
---

## Via an IDE
#### On the command line, execute:
	cd ./VemosReddit && npm start

#### then:
	[IOS]: You can build and run via XCode on attached physical devices or virtual devices
	[Android]: In Android Studio, build the project (happens automatically when opening it) and run the project on an attached or emulated device